package com.sda.sdaapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sda.sdaapp.R;
import com.sda.sdaapp.TextCommunicator;


public class SecondFragment extends Fragment implements TextCommunicator {

    public static final String TAG = "SecondFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void communicate(String textToCommunicate) {
        Toast.makeText(getContext(), textToCommunicate, Toast.LENGTH_SHORT).show();
    }
}
