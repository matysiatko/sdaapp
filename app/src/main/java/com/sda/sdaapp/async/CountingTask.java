package com.sda.sdaapp.async;

import android.os.AsyncTask;

import timber.log.Timber;

public class CountingTask extends AsyncTask<Integer, Integer, String> implements BackgroundTaskContract.Task {

    private BackgroundTaskContract.View view;

    public CountingTask(BackgroundTaskContract.View view) {
        this.view = view;
    }

    @Override
    protected void onPreExecute() {
        // What to do before executing this task
        view.showProgress();
    }

    @Override
    protected String doInBackground(Integer... integers) {
        // Work to do on background thread
        int start = integers[0];
        int stop = integers[1];
        while (start <= stop) {
            Timber.d(String.valueOf(start++));
            publishProgress(start);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return String.format("You've just counted to %d", stop);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // Here you can update the UI, this works on main thread
        view.updateProgress(values[0]);
    }

    @Override
    protected void onPostExecute(String taskResult) {
        // Update your UI on main thread once the task is finished
        view.hideProgress();
        view.showOutput(taskResult);
    }

    @Override
    public void count(Integer from, Integer to) {
        execute(from, to);
    }
}
