package com.sda.sdaapp.async;

public interface BackgroundTaskContract {

    interface View {

        void showProgress();

        void hideProgress();

        void updateProgress(Integer progress);

        void showOutput(String output);
    }

    interface Task {

        void count(Integer from, Integer to);
    }

    interface Presenter {

        void countWithRxJava(Integer from, Integer to);

        void clear();
    }
}
