package com.sda.sdaapp.async;

import android.os.AsyncTask;

import timber.log.Timber;

public class MyAsyncTask extends AsyncTask<Integer, Integer, Void> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Integer... integers) {
        int value = integers[0];
        while (value <= 40) {
            Timber.d(String.valueOf(value++));
            publishProgress(value);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
