package com.sda.sdaapp.spacex.launch.di;

import com.sda.sdaapp.spacex.launch.LaunchActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {LaunchModule.class})
public interface LaunchComponent {

    void inject(LaunchActivity launchActivity);
}
