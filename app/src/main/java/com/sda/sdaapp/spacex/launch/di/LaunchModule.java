package com.sda.sdaapp.spacex.launch.di;

import com.sda.sdaapp.api.Api;
import com.sda.sdaapp.spacex.launch.LaunchContract;
import com.sda.sdaapp.spacex.launch.LaunchPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LaunchModule {

    private LaunchContract.View view;

    public LaunchModule(LaunchContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    LaunchContract.Presenter provideLaunchPresenter(Api api) {
        return new LaunchPresenter(view, api);
    }
}
