package com.sda.sdaapp.spacex;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.sda.sdaapp.data.CompanyInfo;
import com.sda.sdaapp.data.ResponseRockets;
import com.sda.sdaapp.data.Rocket;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import timber.log.Timber;

public class GetCompanyInfoTask extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... strings) {
        String result = "";
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection();

            int responseCode = urlConnection.getResponseCode();
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            result = inputStreamToString(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("API CALL", e.toString());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

//        CompanyInfo companyInfo = parseResponseToModel(result);
        Rocket[] rockets = new Gson().fromJson(result, Rocket[].class);
        Timber.d(result);
        return result;
    }

    private CompanyInfo parseResponseToModel(String result) {
        return new Gson().fromJson(result, CompanyInfo.class);
    }

    private String inputStreamToString(InputStream inputStream) {
        String currentLine;
        StringBuilder response = new StringBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        try {
            while ((currentLine = bufferedReader.readLine()) != null) {
                response.append(currentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
