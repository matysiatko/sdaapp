package com.sda.sdaapp.spacex.rocket;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.sda.sdaapp.api.Api;
import com.sda.sdaapp.data.CompanyInfo;
import com.sda.sdaapp.data.Rocket;

import java.util.List;
import java.util.PrimitiveIterator;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class RocketPresenter implements RocketContract.Presenter, LifecycleObserver {

    private RocketContract.View view;
    private Api api;

    private CompanyInfo companyInfo;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public RocketPresenter(RocketContract.View view, Api api) {
        this.view = view;
        this.api = api;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        getCompanyInfo();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getRockets();
        getLaunchesByYear(2018);
    }

    @Override
    public void onTryAgainClick() {
        getRockets();
    }

    private void getCompanyInfo() {
        compositeDisposable.add(api.getCompanyInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        companyInfo -> {
                            // onNext
                            this.companyInfo = companyInfo;
                            Timber.e(companyInfo.getHeadquarter().getAddress());
                        },
                        throwable -> {
                            // onError
                            Timber.e(throwable.getMessage());
                        },
                        () -> {
                            // onCompleted
                        }
                )
        );
    }

    @Override
    public void getRockets() {
        view.showProgress();
        compositeDisposable.add(
                api.getRockets()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                rockets -> view.showData(rockets),
                                throwable -> view.showError()
                        )
        );
    }

    private void getLaunchesByYear(int year) {
        compositeDisposable.add(
                api.getLaunchesByYear(year)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                launches -> {
                                    if (!launches.isEmpty()) {
                                        Timber.e(String.valueOf(launches.get(0).getFlightNumber()));
                                    }
                                },
                                Throwable::printStackTrace
                        )
        );
    }
}
