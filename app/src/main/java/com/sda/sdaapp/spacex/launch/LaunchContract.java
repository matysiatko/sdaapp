package com.sda.sdaapp.spacex.launch;

import com.sda.sdaapp.data.Launch;

import java.util.List;

public interface LaunchContract {

    interface View {

        void showProgress();

        void showError();

        void showLaunches(List<Launch> launches);
    }

    interface Presenter {

        void setRocketId(String rocketId);
    }
}
