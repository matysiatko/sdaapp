package com.sda.sdaapp.spacex.launch;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sda.sdaapp.R;
import com.sda.sdaapp.data.Launch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaunchAdapter extends RecyclerView.Adapter<LaunchAdapter.ViewHolder> {

    private List<Launch> launches = new ArrayList<>();

    public void updateLaunches(List<Launch> launchList) {
        launches.clear();
        launches.addAll(launchList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_launch, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setup(launches.get(position));
    }

    @Override
    public int getItemCount() {
        return launches.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_launch_image)
        ImageView launchImage;

        @BindView(R.id.item_launch_description)
        TextView launchDescription;

        @BindView(R.id.item_launch_date)
        TextView launchDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setup(Launch launch) {
            Glide.with(itemView.getContext())
                    .load(launch.getLink().getImageUrl())
                    .into(launchImage);

            launchDescription.setText(launch.getDetails());

            SimpleDateFormat inputFormat = new SimpleDateFormat(Launch.DATE_FORMAT, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat(Launch.DATE_OUTPUT, Locale.getDefault());

            try {
                Date date = inputFormat.parse(launch.getDate());
                launchDate.setText(outputFormat.format(date));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
