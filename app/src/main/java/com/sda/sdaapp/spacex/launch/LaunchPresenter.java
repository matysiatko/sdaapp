package com.sda.sdaapp.spacex.launch;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.sda.sdaapp.api.Api;
import com.sda.sdaapp.data.Launch;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

public class LaunchPresenter implements LaunchContract.Presenter, LifecycleObserver {

    private LaunchContract.View view;
    private Api api;
    private String rocketId;
    private Realm realm;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LaunchPresenter(LaunchContract.View view, Api api) {
        this.view = view;
        this.api = api;

        ((LifecycleOwner) view).getLifecycle().addObserver(this);
    }

    @Override
    public void setRocketId(String rocketId) {
        this.rocketId = rocketId;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void getLaunchesForRocket() {
        view.showProgress();

        RealmResults<Launch> launchRealmResults = realm
                .where(Launch.class)
                .equalTo("rocketId", rocketId)
                .findAll();

        if (launchRealmResults.isEmpty()) {
            compositeDisposable.add(
                    api.getAllLaunchesForRocket(rocketId)
                            // on which thread pool will we start the chain
                            .subscribeOn(Schedulers.io())
                            .flatMap(launches -> {
                                // add rocket id to each launch object
                                for (Launch launch : launches) {
                                    launch.setRocketId(rocketId);
                                }

                                // return single of launches (rx) for further operations in the chain
                                return Observable.just(launches);
                            })
                            // switch to the "UI" thread
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    launches -> {
                                        // save all launches into Realm database
                                        realm.executeTransaction(realm -> realm.copyToRealmOrUpdate(launches));

                                        view.showLaunches(launches);
                                    },
                                    throwable -> view.showError())
            );
        } else {
            view.showLaunches(launchRealmResults);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        realm = Realm.getDefaultInstance();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
        realm.close();
        realm = null;
    }
}
