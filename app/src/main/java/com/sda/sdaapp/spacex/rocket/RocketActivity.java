package com.sda.sdaapp.spacex.rocket;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.sda.sdaapp.R;
import com.sda.sdaapp.api.Api;
import com.sda.sdaapp.data.Rocket;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RocketActivity extends AppCompatActivity implements RocketContract.View {

    @BindView(R.id.spaces_rocket_reycler)
    RecyclerView rocketRecycler;

    @BindView(R.id.spacex_rocket_progress)
    ProgressBar rocketProgress;

    @BindView(R.id.spacex_rocket_error_group)
    Group rocketErrorGroup;

    @BindView(R.id.spacex_rocket_refresh)
    SwipeRefreshLayout rocketRefresh;

    @BindView(R.id.spacex_rocket_toolbar)
    Toolbar rocketToolbar;

    @OnClick(R.id.spacex_rocket_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    private RocketContract.Presenter presenter;
    private RocketAdapter rocketAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spacex_rocket);
        ButterKnife.bind(this);

        setSupportActionBar(rocketToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.spacex_rockets);
        }

        Retrofit retrofit = new Retrofit.Builder()
                // set base url for every request method defined in Api.class
                .baseUrl(Api.BASE_URL)
                // add Gson converter to deserialize JSON into POJO
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                // add RxJava2 call adapter
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                // return Retrofit instance
                .build();

        presenter = new RocketPresenter(
                this,
                // create Api interface instance
                retrofit.create(Api.class)
        );

        rocketAdapter = new RocketAdapter();
        rocketRecycler.setLayoutManager(new LinearLayoutManager(this));
        rocketRecycler.setAdapter(rocketAdapter);

        rocketRefresh.setOnRefreshListener(() -> presenter.getRockets());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.menu_spacex_refresh) {
            presenter.getRockets();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_spacex_rocket, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void showError() {
        rocketProgress.setVisibility(View.INVISIBLE);
        rocketErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Rocket> rockets) {
        rocketRefresh.setRefreshing(false);
        rocketProgress.setVisibility(View.INVISIBLE);
        rocketRecycler.setVisibility(View.VISIBLE);
        rocketAdapter.updateRockets(rockets);
    }

    @Override
    public void showProgress() {
        rocketRecycler.setVisibility(View.INVISIBLE);
        rocketErrorGroup.setVisibility(View.INVISIBLE);
        rocketProgress.setVisibility(View.VISIBLE);
    }
}
