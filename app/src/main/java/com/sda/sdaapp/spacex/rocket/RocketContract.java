package com.sda.sdaapp.spacex.rocket;

import com.sda.sdaapp.data.Rocket;

import java.util.List;

public interface RocketContract {

    interface View {

        void showError();

        void showData(List<Rocket> rockets);

        void showProgress();
    }

    interface Presenter {

        void getRockets();

        void onTryAgainClick();
    }
}
