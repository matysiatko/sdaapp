package com.sda.sdaapp.spacex.launch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.sda.sdaapp.R;
import com.sda.sdaapp.SdaApplication;
import com.sda.sdaapp.api.Api;
import com.sda.sdaapp.data.Launch;
import com.sda.sdaapp.spacex.launch.di.LaunchModule;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class LaunchActivity extends AppCompatActivity implements LaunchContract.View {

    public static final String KEY_ROCKET_ID = "rocketId";

    @BindView(R.id.launch_recycler)
    RecyclerView launchRecycler;

    @BindView(R.id.launch_progress)
    ProgressBar launchProgress;

    @BindView(R.id.launch_error_group)
    Group launchErrorGroup;

    @Inject
    LaunchContract.Presenter presenter;

    private LaunchAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        ButterKnife.bind(this);

        ((SdaApplication) getApplication())
                .getAppComponent()
                .plus(new LaunchModule(this))
                .inject(this);

        adapter = new LaunchAdapter();
        launchRecycler.setLayoutManager(new LinearLayoutManager(this));
        launchRecycler.setAdapter(adapter);

        presenter.setRocketId(getIntent().getStringExtra(KEY_ROCKET_ID));
    }

    @Override
    public void showProgress() {
        launchRecycler.setVisibility(View.INVISIBLE);
        launchErrorGroup.setVisibility(View.INVISIBLE);
        launchProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        launchProgress.setVisibility(View.INVISIBLE);
        launchErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLaunches(List<Launch> launches) {
        adapter.updateLaunches(launches);

        launchErrorGroup.setVisibility(View.INVISIBLE);
        launchProgress.setVisibility(View.INVISIBLE);
        launchRecycler.setVisibility(View.VISIBLE);
    }
}
