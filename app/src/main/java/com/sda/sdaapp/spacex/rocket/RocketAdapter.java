package com.sda.sdaapp.spacex.rocket;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sda.sdaapp.R;
import com.sda.sdaapp.data.Rocket;
import com.sda.sdaapp.spacex.launch.LaunchActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RocketAdapter extends RecyclerView.Adapter<RocketAdapter.ViewHolder> {

    private List<Rocket> rocketList = new ArrayList<>();

    public void updateRockets(List<Rocket> rockets) {
        rocketList.clear();
        rocketList.addAll(rockets);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_rocket, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupRocket(rocketList.get(position));
    }

    @Override
    public int getItemCount() {
        return rocketList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_rocket_name)
        TextView rocketName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupRocket(Rocket rocket) {
            rocketName.setText(rocket.name);

            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), LaunchActivity.class);
                intent.putExtra(LaunchActivity.KEY_ROCKET_ID, rocket.id);

                itemView.getContext().startActivity(intent);
            });
        }
    }
}
