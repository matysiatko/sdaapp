package com.sda.sdaapp.data;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsManager {

    public static final String PREFS_FILE_NAME = "com.sda.sdaapp.PREFS_FILE";
    private static final String STUDENTS_COUNT = "students_count";
    public static final String FIREBASE_FCM_TOKEN = "firebase regisration token";

    private SharedPreferences sharedPreferences;

    public PrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void saveStudentsCount(int count) {
        sharedPreferences
                .edit()
                .putInt(STUDENTS_COUNT, count)
                .apply();
    }

    public int getStudentsCount() {
        return sharedPreferences.getInt(STUDENTS_COUNT, 0);
    }
}
