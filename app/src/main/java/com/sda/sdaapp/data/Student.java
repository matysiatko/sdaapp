package com.sda.sdaapp.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Student {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String name;

    public String surname;

    @ColumnInfo(name = "is_drunk")
    public boolean isDrunk;

    public float grade;

    @ColumnInfo(name = "account_value")
    public float accountValue;

    @Embedded
    public Address address = new Address();
}
