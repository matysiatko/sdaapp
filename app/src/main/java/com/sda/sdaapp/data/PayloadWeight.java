package com.sda.sdaapp.data;

class PayloadWeight {

    public String id;
    public String name;
    public Integer kg;
    public Integer lb;
}
