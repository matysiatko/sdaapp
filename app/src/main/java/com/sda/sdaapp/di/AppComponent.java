package com.sda.sdaapp.di;

import com.sda.sdaapp.ApplicationScope;
import com.sda.sdaapp.spacex.launch.LaunchContract;
import com.sda.sdaapp.spacex.launch.di.LaunchComponent;
import com.sda.sdaapp.spacex.launch.di.LaunchModule;
import com.sda.sdaapp.students.StudentsActivity;
import com.sda.sdaapp.students.StudentsComponent;
import com.sda.sdaapp.students.StudentsModule;
import com.sda.sdaapp.students.edit.di.EditStudentComponent;
import com.sda.sdaapp.students.edit.di.EditStudentModule;

import javax.inject.Singleton;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, DataModule.class})
public interface AppComponent {

    StudentsComponent plus(StudentsModule studentsModule);

    EditStudentComponent plus(EditStudentModule editStudentModule);

    LaunchComponent plus(LaunchModule launchModule);
}
