package com.sda.sdaapp.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.google.gson.Gson;
import com.sda.sdaapp.ApplicationScope;
import com.sda.sdaapp.api.Api;
import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.data.PrefsManager;
import com.sda.sdaapp.room.SdaDatabase;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {

    @Provides
    @ApplicationScope
    PrefsManager providePrefsManager(Context context) {
        return new PrefsManager(context);
    }

    @Provides
    @ApplicationScope
    SdaDatabase provideSdaDatabase(Context context) {
        return Room.databaseBuilder(context, SdaDatabase.class, "sda-room-database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @ApplicationScope
    StudentDao provideStudentDao(SdaDatabase sdaDatabase) {
        return sdaDatabase.studentDao();
    }

    @Provides
    @ApplicationScope
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @ApplicationScope
    Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }
}
