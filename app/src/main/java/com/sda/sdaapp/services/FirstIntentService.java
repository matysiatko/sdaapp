package com.sda.sdaapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import timber.log.Timber;


public class FirstIntentService extends IntentService {

    public static final String SERVICE_BROADCAST_ACTION = "akcja po której rozpoznaję broadcast";
    public static final String SERVICE_CURRENT_SECOND = "current_second";

    public FirstIntentService() {
        super("FirstIntentService");
        Timber.d("Constructor");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("onDestroy");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Timber.d("onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Timber.d("onBind");
        return super.onBind(intent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Timber.d("Service is working now!");

        for (int i = 1; i <= 30; i++) {
            Intent intentToSend = new Intent();
            intentToSend.setAction(SERVICE_BROADCAST_ACTION);
            intentToSend.putExtra(SERVICE_CURRENT_SECOND, i);

            LocalBroadcastManager.getInstance(this).sendBroadcast(intentToSend);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
