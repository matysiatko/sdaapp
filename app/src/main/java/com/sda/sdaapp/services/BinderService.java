package com.sda.sdaapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Random;


public class BinderService extends Service {

    private MyBinder binder = new MyBinder();
    private Random numberGenerator = new Random();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class MyBinder extends Binder {

        public BinderService getService() {
            return BinderService.this;
        }
    }

    public String greet() {
        return String.format("Hello my friend! Your lucky number is %d", numberGenerator.nextInt(10));
    }
}
