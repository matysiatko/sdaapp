package com.sda.sdaapp.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobService extends JobService {

    public static final int MY_JOB_ID = 123;

    private Disposable disposable;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        // runs on the main thread by default
        // kick off background task and return true
        count(0, 50, jobParameters);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        // return true if you'd like the system to reschedule the job
        disposable.dispose();
        return true;
    }

    private void count(Integer from, Integer to, final JobParameters jobParameters) {
        disposable = Flowable.create((FlowableOnSubscribe<Integer>) emitter -> {
            Integer i = from;
            while (i <= to) {
                Timber.d(String.valueOf(i++));
                emitter.onNext(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            emitter.onComplete();
        }, BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.computation())
                .subscribe(
                        currentProgress -> {
                            Log.e("JOB", currentProgress.toString());
                            // todo send broadcast
                        }, throwable -> {
                            jobFinished(jobParameters, false);
                        }, () -> {
                            jobFinished(jobParameters, false);
                        });
    }
}
