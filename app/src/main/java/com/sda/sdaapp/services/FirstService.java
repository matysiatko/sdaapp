package com.sda.sdaapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.sda.sdaapp.async.MyAsyncTask;

import timber.log.Timber;


public class FirstService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("Never-ending service created");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("Never-ending service destroyed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startCounting();
        return super.onStartCommand(intent, flags, startId);
    }

    private void startCounting() {
        new MyAsyncTask().execute(2);
    }
}
