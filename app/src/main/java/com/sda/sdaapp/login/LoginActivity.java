package com.sda.sdaapp.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.sda.sdaapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @BindView(R.id.login_email)
    TextInputLayout loginEmail;

    @BindView(R.id.login_password)
    TextInputLayout loginPassword;

    private LoginContract.Presenter presenter;

    @OnClick(R.id.login_button)
    public void onButtonClick() {
        presenter.onLoginButtonClick(
                loginEmail.getEditText().getText().toString(),
                loginPassword.getEditText().getText().toString()
        );
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        presenter = new LoginPresenter(this);
    }

    @Override
    public void showEmailError(int errorMessageId) {
        loginEmail.setError(getString(errorMessageId));
    }

    @Override
    public void showPasswordError(int errorMessageId) {
        loginPassword.setError(getString(errorMessageId));
    }

    @Override
    public void logIn() {
        Toast.makeText(this, R.string.login_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void clearErrors() {
        loginEmail.setError(null);
        loginPassword.setError(null);
    }
}
