package com.sda.sdaapp.activities.schedulejob;

public interface ScheduleJobContract {

    interface View {

        void scheduleJob();
    }

    interface Presenter {

        void onButtonClick();
    }
}
