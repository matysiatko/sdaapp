package com.sda.sdaapp.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sda.sdaapp.R;
import com.sda.sdaapp.camera.CameraActivity;
import com.sda.sdaapp.students.StudentsActivity;
import com.sda.sdaapp.drawer.DrawerActivity;
import com.sda.sdaapp.files.InternalStorageActivity;
import com.sda.sdaapp.login.LoginActivity;
import com.sda.sdaapp.receivers.AlarmSetterReceiver;
import com.sda.sdaapp.services.BinderService;
import com.sda.sdaapp.services.FirstIntentService;
import com.sda.sdaapp.services.FirstService;
import com.sda.sdaapp.services.ForegroundService;
import com.sda.sdaapp.spacex.rocket.RocketActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ScrollActivity extends AppCompatActivity {

    @BindView(R.id.scroll_counting_progress)
    ProgressBar countingProgress;

    public static final String ALARM_BROADCAST_ACTION = "com.sda.sdaapp.action.ALARM_BROADCAST";
    public static final String NOTIFICATION_CHANNEL_ID = "channelId";
    public static final String NOTIFICATION_CHANNEL_NAME = "Channel Sda App";
    public static final int NOTIFICATION_ID = 151900;

    private AlarmSetterReceiver alarmSetterReceiver = new AlarmSetterReceiver();
    private BroadcastReceiver countingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            countingProgress.setProgress(
                    intent.getIntExtra(FirstIntentService.SERVICE_CURRENT_SECOND, 0)
            );
        }
    };

    private BinderService binderService;
    private boolean isServiceBound;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrollview);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, FirstService.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerForLocalReceiver();
        registerForCountingReceiver();
        bindToService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterLocalReceiver();
        unregisterForCountingReceiver();
        unbindFromService();
    }

    @OnClick(R.id.scroll_button_send_locally)
    public void sendLocalBroadcast() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.BROADCAST_ACTION);
        intent.putExtra(MainActivity.BROADCAST_KEY, "Hey! This is a local broadcast from Scroll!");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @OnClick(R.id.scroll_button_send_locally_set_alarm)
    public void sendBroadcastToSetAlarm() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ALARM_BROADCAST_ACTION));
    }

    @OnClick(R.id.scroll_button_show_notification)
    public void onShowNotificationClick() {
        showSimpleNotification();
    }

    @OnClick(R.id.scroll_button_start_intent_service)
    public void onStartIntentServiceClick() {
        startService(new Intent(this, FirstIntentService.class));
    }

    @OnClick(R.id.scroll_button_start_service)
    public void onStartServiceClick() {
        startService(new Intent(this, FirstService.class));
    }

    @OnClick(R.id.scroll_button_start_foreground_service)
    public void onStartForegroundServiceClick() {
        Intent intent = new Intent(this, ForegroundService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
    }

    @OnClick(R.id.scroll_button_get_value_from_bound_service)
    public void onDrawNumberClick() {
        if (isServiceBound) {
            Toast.makeText(this, binderService.greet(), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.scroll_button_students)
    public void onStudentsClick() {
        startActivity(new Intent(this, StudentsActivity.class));
    }

    @OnClick(R.id.scroll_button_rockets)
    public void onRocketsClick() {
        startActivity(new Intent(this, RocketActivity.class));
    }

    @OnClick(R.id.scroll_button_login)
    public void onLoginClick() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @OnClick(R.id.scroll_button_pages)
    public void onPagesClick() {
        startActivity(new Intent(this, PagesActivity.class));
    }

    @OnClick(R.id.scroll_button_drawer)
    public void onDrawerClick() {
        startActivity(new Intent(this, DrawerActivity.class));
    }

    @OnClick(R.id.scroll_button_internal_storage)
    public void onInternalClick() {
        startActivity(new Intent(this, InternalStorageActivity.class));
    }

    @OnClick(R.id.scroll_button_camera)
    public void onCameraClick() {
        startActivity(new Intent(this, CameraActivity.class));
    }

    private void registerForLocalReceiver() {
        IntentFilter intentFilter = new IntentFilter(ALARM_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(alarmSetterReceiver, intentFilter);
    }

    private void unregisterLocalReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(alarmSetterReceiver);
    }

    private void registerForCountingReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                countingReceiver,
                new IntentFilter(FirstIntentService.SERVICE_BROADCAST_ACTION)
        );
    }

    private void unregisterForCountingReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(countingReceiver);
    }

    private void showSimpleNotification() {
        Intent intent = new Intent(this, PagesActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        PendingIntent pendingIntentWithParentStack = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_cloud)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_message))
                .setContentIntent(pendingIntentWithParentStack)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager notificationManager = (NotificationManager) getSystemService
                    (Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    private void bindToService() {
        bindService(new Intent(this, BinderService.class), serviceConnection, Context
                .BIND_AUTO_CREATE);
    }

    private void unbindFromService() {
        unbindService(serviceConnection);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            BinderService.MyBinder binder = (BinderService.MyBinder) iBinder;
            binderService = binder.getService();
            isServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isServiceBound = false;
        }
    };
}
