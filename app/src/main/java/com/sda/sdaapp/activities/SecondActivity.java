package com.sda.sdaapp.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sda.sdaapp.R;


public class SecondActivity extends AppCompatActivity {

    private TextView textView;
    private Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView = (TextView) findViewById(R.id.textView2);
        button = (Button) findViewById(R.id.button2);

        final Intent intent = getIntent();
        final String textSentFromFirstActivity = intent.getStringExtra(MainActivity.KEY_DATA);
        textView.setText(textSentFromFirstActivity);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = textSentFromFirstActivity;
                if (!textSentFromFirstActivity.startsWith("http://") && !textSentFromFirstActivity.startsWith("https://")) {
                    url = "http://" + url;
                }
                Intent implicitIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

                if (implicitIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(implicitIntent);
                }
            }
        });
    }
}
