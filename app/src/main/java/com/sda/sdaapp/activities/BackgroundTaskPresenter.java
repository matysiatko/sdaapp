package com.sda.sdaapp.activities;

import com.sda.sdaapp.async.BackgroundTaskContract;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class BackgroundTaskPresenter implements BackgroundTaskContract.Presenter {

    private BackgroundTaskContract.View view;
    private Disposable countingDisposable;
    private String output = "";
    private Integer value;

    public BackgroundTaskPresenter(BackgroundTaskContract.View view) {
        this.view = view;
    }

    @Override
    public void countWithRxJava(Integer from, Integer to) {
        view.showProgress();
        countingDisposable = getCountingObservable(from, to)
                .flatMap(integer -> {
                    value = integer;
                    // make another thing once counting is done, we will pass Strings down the chain
                    // to do that, we use flatMap operator that need to return an Observable
                    return getStringsObservable();
                })
                // this informs on which thread work needs to be done once subscribe is invoked
                // on the observable
                .subscribeOn(Schedulers.computation())
                // this changes threads in the chain, may be used multiple times
                // views may be updated only from Main Thread, so we use RxAndroid to have access
                // to AndroidSchedulers class
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        // onNext
                        text -> output += text,
                        // onError
                        Throwable::printStackTrace,
                        // onCompleted
                        () -> {
                            view.showOutput(output + value.toString());
                            view.hideProgress();
                        }
                );
    }

    @Override
    public void clear() {
        countingDisposable.dispose();
    }

    private Observable<Integer> getCountingObservable(final Integer from, Integer to) {
        return Observable.fromCallable(() -> {
            Integer start = from;
            while (start <= to) {
                Timber.d(String.valueOf(start++));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return start;
        });
    }

    private Observable<String> getStringsObservable() {
        return Observable.just("One ", "Two ", "Three ");
    }

}
