package com.sda.sdaapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sda.sdaapp.R;
import com.sda.sdaapp.camera.CameraActivity;
import com.sda.sdaapp.receivers.CustomReceiver;
import com.sda.sdaapp.services.ForegroundService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_DATA = "key_data";
    public static final String SAVED_INPUT = "saved_input";
    public static final String BROADCAST_ACTION = "com.sda.sdaapp.CUSTOM_ACTION";
    public static final String BROADCAST_KEY = "key used to send string via broadcast";

    private Context context = this;

    private BroadcastReceiver localBroadcastSentFromScroll = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onLocalBroadcastReceived(intent);
        }
    };

    @BindView(R.id.edit_text)
    EditText editText;

    @BindView(R.id.button)
    Button colorButton;

    @BindView(R.id.open_camera_button)
    Button cameraButton;

    @BindView(R.id.open_webview_button)
    Button webviewButton;

    @BindView(R.id.open_email_button)
    Button emailButton;

    @OnClick(R.id.send_custom_broadcast)
    public void sendCustomBroadcast() {
        Intent intent = new Intent(this, CustomReceiver.class);
        intent.setAction(CustomReceiver.ACTION);
        sendBroadcast(intent);
    }

    @OnClick(R.id.open_scroll_button)
    public void openScrollActivity() {
        startActivity(new Intent(this, ScrollActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Restoring activity state after re-creation of the activity
        if (savedInstanceState != null) {
            colorButton.setBackgroundColor(savedInstanceState.getInt(SAVED_INPUT));
        }

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCameraActivity();
            }
        });

        webviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWebView();
            }
        });

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEmailApp();
            }
        });

        registerForLocalReceiver();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.main_menu_share);
        android.support.v7.widget.ShareActionProvider provider = (android.support.v7.widget.ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
        provider.setShareIntent(getShareIntent());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu_camera: {
                openCameraActivity();
            }
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Get button background and save it in the bundle to restore the state later, during re-creation
        ColorDrawable colorDrawable = (ColorDrawable) colorButton.getBackground();
        outState.putInt(SAVED_INPUT, colorDrawable.getColor());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Timber.d("onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Timber.d("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterLocalReceiver();
        stopForegroundService();
        Timber.d("onDestroy");
    }

    @OnClick(R.id.button)
    public void changeButtonStyle() {
        colorButton.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
    }

    public void openCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    private void openSecondActivity() {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(KEY_DATA, editText.getText().toString());
        startActivity(intent);
    }

    private void openWebView() {
        Intent intent = new Intent(this, WebActivity.class);
        intent.putExtra(WebActivity.WEB_URL, editText.getText().toString());
        startActivity(intent);
    }

    private void openEmailApp() {
        startActivity(Intent.createChooser(getShareIntent(), "Title of chooser"));
    }

    private Intent getShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@sdacademy.pl"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Hello from android course");
        return intent;
    }

    private void registerForLocalReceiver() {
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(localBroadcastSentFromScroll, intentFilter);
    }

    private void unregisterLocalReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(localBroadcastSentFromScroll);
    }

    private void onLocalBroadcastReceived(Intent intent) {
        editText.setText(intent.getStringExtra(BROADCAST_KEY));
    }

    private void stopForegroundService() {
        stopService(new Intent(this, ForegroundService.class));
    }
}
