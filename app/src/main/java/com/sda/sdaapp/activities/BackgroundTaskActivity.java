package com.sda.sdaapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sda.sdaapp.R;
import com.sda.sdaapp.async.BackgroundTaskContract;
import com.sda.sdaapp.async.CountingTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BackgroundTaskActivity extends AppCompatActivity implements BackgroundTaskContract.View {

    public static final int START = 10;
    public static final int STOP = 15;

    private BackgroundTaskContract.Task task;
    private BackgroundTaskContract.Presenter presenter;

    @BindView(R.id.task_output)
    TextView taskOutput;

    @BindView(R.id.task_progress)
    ProgressBar taskProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background_task);
        ButterKnife.bind(this);

        task = new CountingTask(this);
        presenter = new BackgroundTaskPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startBackgroundCounting();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.clear();
    }

    @Override
    public void showProgress() {
        taskProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        taskProgress.setVisibility(View.GONE);
    }

    @Override
    public void updateProgress(Integer progress) {
        taskProgress.setProgress(progress);
    }

    @Override
    public void showOutput(String output) {
        taskOutput.setText(output);
    }

    private void startBackgroundCounting() {
        taskProgress.setMax(STOP);
//        task.count(START, STOP);
        presenter.countWithRxJava(START, STOP);
    }
}
