package com.sda.sdaapp.activities.schedulejob;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.sda.sdaapp.R;
import com.sda.sdaapp.services.MyJobService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScheduleJobActivity extends AppCompatActivity implements ScheduleJobContract.View {

    @BindView(R.id.job_progress)
    ProgressBar jobProgress;

    @OnClick(R.id.job_button)
    public void onButtonClick() {
        presenter.onButtonClick();
    }

    private ScheduleJobContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_job);
        ButterKnife.bind(this);
        presenter = new ScheduleJobPresenter(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void scheduleJob() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.schedule(
                    new JobInfo.Builder(
                            MyJobService.MY_JOB_ID, new ComponentName(this, MyJobService.class))
                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                            .setRequiresCharging(true)
                            .build()
            );
        }
    }
}
