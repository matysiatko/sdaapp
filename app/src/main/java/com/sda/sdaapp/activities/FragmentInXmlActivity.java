package com.sda.sdaapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.sda.sdaapp.R;
import com.sda.sdaapp.TextCommunicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class FragmentInXmlActivity extends AppCompatActivity {

    @BindView(R.id.host_input)
    EditText input;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_in_xml);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.host_button)
    public void onButtonClick() {
        TextCommunicator communicator1 = (TextCommunicator) getSupportFragmentManager().findFragmentById(R.id.host_fragment_1);
        TextCommunicator communicator2 = (TextCommunicator) getSupportFragmentManager().findFragmentById(R.id.host_fragment_2);
        communicator1.communicate(input.getText().toString());
        communicator2.communicate("Hey, how are you?");
    }
}
