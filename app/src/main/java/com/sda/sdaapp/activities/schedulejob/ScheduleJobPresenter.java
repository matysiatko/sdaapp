package com.sda.sdaapp.activities.schedulejob;

public class ScheduleJobPresenter implements ScheduleJobContract.Presenter {

    private ScheduleJobContract.View view;

    ScheduleJobPresenter(ScheduleJobContract.View view) {
        this.view = view;
    }

    @Override
    public void onButtonClick() {
        view.scheduleJob();
    }
}
