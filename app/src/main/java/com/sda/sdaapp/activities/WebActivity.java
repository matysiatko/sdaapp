package com.sda.sdaapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sda.sdaapp.R;


public class WebActivity extends AppCompatActivity {

    public static final String WEB_URL = "WEB_URL";

    WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        String url = getIntent().getStringExtra(WEB_URL);

        webView = (WebView) findViewById(R.id.web_wv);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
    }
}
