package com.sda.sdaapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sda.sdaapp.fragments.FirstFragment;
import com.sda.sdaapp.R;
import com.sda.sdaapp.fragments.SecondFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class DynamicFragmentsActivity extends AppCompatActivity {

    private List<Fragment> fragmentList = new ArrayList<>();
    private boolean isFirstDisplayed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamics_fragment);
        ButterKnife.bind(this);
        fragmentList.add(new FirstFragment());
        fragmentList.add(new SecondFragment());
        changeFragment(FirstFragment.TAG);
    }

    @OnClick(R.id.dynamic_replace_button)
    public void onAddButtonClick(View view) {
        if (isFirstDisplayed) {
            changeFragment(SecondFragment.TAG);
        } else {
            changeFragment(FirstFragment.TAG);
        }
    }

    private void changeFragment(String fragmentTag) {
        Fragment fragmentToShow;
        if (fragmentTag.equals(FirstFragment.TAG)) {
            fragmentToShow = fragmentList.get(0);
            isFirstDisplayed = true;
        } else {
            fragmentToShow = fragmentList.get(1);
            isFirstDisplayed = false;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.dynamic_container, fragmentToShow)
                .commit();
    }
}
