package com.sda.sdaapp.files;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.sda.sdaapp.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InternalStorageActivity extends AppCompatActivity {

    @BindView(R.id.internal_file_text)
    TextView fileTextContent;

    public static final String FILE_NAME = "my_internal_file.txt";
    public static final String FILE_CONTENT = "Hello, this is save internally!";

    private File file = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal_storage);
        ButterKnife.bind(this);

        createFileInInternalStorage();
        saveFileInternalStorage();
        readTextFromFile();
    }

    private void createFileInInternalStorage() {
        File internalDirectory = getFilesDir();
        file = new File(internalDirectory, FILE_NAME);
    }

    private void saveFileInternalStorage() {
        try {
            FileOutputStream outputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            outputStream.write(FILE_CONTENT.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readTextFromFile() {
        byte[] bytes = new byte[(int) file.length()];
        try {
            FileInputStream inputStream = openFileInput(FILE_NAME);
            inputStream.read(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fileTextContent.setText(new String(bytes));
    }
}
