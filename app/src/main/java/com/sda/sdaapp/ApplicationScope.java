package com.sda.sdaapp;

import javax.inject.Scope;

@Scope
public @interface ApplicationScope {
}
