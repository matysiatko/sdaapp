package com.sda.sdaapp.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sda.sdaapp.data.PrefsManager;

public class SdaInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get refresh token from Firebase
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e("ON TOKEN REFRESH", "Token: " + token);

        // Save it to SharedPreferences
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences
                (PrefsManager.PREFS_FILE_NAME, Context.MODE_PRIVATE);

        sharedPreferences
                .edit()
                .putString(PrefsManager.FIREBASE_FCM_TOKEN, token)
                .apply();
    }
}