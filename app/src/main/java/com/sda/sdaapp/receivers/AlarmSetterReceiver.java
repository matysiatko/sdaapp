package com.sda.sdaapp.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.sda.sdaapp.activities.PagesActivity;


public class AlarmSetterReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intentToStartAfterAlarmSetsOff = new Intent(
                context,
                PagesActivity.class
        );
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                intentToStartAfterAlarmSetsOff,
                0
        );

        if (alarmManager != null) {
            alarmManager.set(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 5 * 1000,
                    pendingIntent
            );
        }
    }
}
