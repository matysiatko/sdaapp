package com.sda.sdaapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sda.sdaapp.activities.PagesActivity;


public class CustomReceiver extends BroadcastReceiver {

    public static final String ACTION = "com.sda.saaapp.receivers.ACTION_CUSTOM";

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startActivity(new Intent(context, PagesActivity.class));
    }
}
