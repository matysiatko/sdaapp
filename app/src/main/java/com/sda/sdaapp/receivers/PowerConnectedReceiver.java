package com.sda.sdaapp.receivers;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;


public class PowerConnectedReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        String language;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            language = context.getResources().getConfiguration().getLocales().get(0).getDisplayLanguage();
        } else {
            language = context.getResources().getConfiguration().locale.getDisplayLanguage();
        }
        Toast.makeText(context, language, Toast.LENGTH_SHORT).show();
    }
}
