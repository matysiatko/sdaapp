package com.sda.sdaapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sda.sdaapp.activities.DynamicFragmentsActivity;


public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startActivity(new Intent(context, DynamicFragmentsActivity.class));
    }
}
