package com.sda.sdaapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sda.sdaapp.services.ForegroundService;


public class ForegroundNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals(ForegroundService.NOTIFICATION_ACTION_STOP_SERVICE)) {
            context.stopService(new Intent(context, ForegroundService.class));
        }
    }
}
