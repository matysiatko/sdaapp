package com.sda.sdaapp.students.edit;

import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.data.Student;
import com.sda.sdaapp.students.edit.EditStudentContract.Presenter;

public class EditStudentPresenter implements Presenter {

    private EditStudentContract.View view;
    private StudentDao studentDao;
    private boolean isEditMode;
    private Student student = new Student();

    public EditStudentPresenter(EditStudentContract.View view, StudentDao studentDao) {
        this.view = view;
        this.studentDao = studentDao;
    }

    @Override
    public void handleStudent(int studentId) {
        if (studentId >= 0) {
            // this id exist in the database, so get student and display its data
            isEditMode = true;
            student = studentDao.getStudentById(studentId);
            view.showStudentData(student);
        }
    }

    @Override
    public void onSaveClick(String name, String surname, String accountValue, String grade,
                            String city, String street, String homeNumber, boolean isDrunk) {
        if (isAddressValid(city, street, homeNumber)) {
            student.name = name;
            student.surname = surname;
            if (!accountValue.isEmpty()) {
                student.accountValue = Float.valueOf(accountValue);
            }
            if (!grade.isEmpty()) {
                student.grade = Float.valueOf(grade);
            }
            student.address.city = city;
            student.address.street = street;
            student.address.number = Integer.valueOf(homeNumber);
            student.isDrunk = isDrunk;

            if (isEditMode) {
                if (studentDao.update(student) >= 0) {
                    view.close();
                } else {
                    view.showError();
                }
            } else {
                if (studentDao.insert(student) >= 0) {
                    view.close();
                } else {
                    view.showError();
                }
            }
        } else {
            view.showMissingAddress();
        }
    }

    private boolean isAddressValid(String city, String street, String homeNumber) {
        return !city.isEmpty() && !street.isEmpty() && !homeNumber.isEmpty();
    }
}
