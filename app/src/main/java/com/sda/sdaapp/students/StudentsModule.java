package com.sda.sdaapp.students;

import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.data.PrefsManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StudentsModule {

    private StudentsContract.View view;

    public StudentsModule(StudentsContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    StudentsContract.Presenter provideStudentPresenter(PrefsManager prefsManager, StudentDao studentDao) {
        return new StudentsPresenter(view, prefsManager, studentDao);
}
}
