package com.sda.sdaapp.students;

import com.sda.sdaapp.data.Student;

import java.util.List;

public interface StudentsContract {

    interface View {

        void updateList(List<Student> students);

        void showStudentRemoved();

        void undoRemoval(int position, Student student);

        void showToast();
    }

    interface Presenter {

        void getStudentsCountFromSharedPreferences();

        void getStudentsFromRoom();

        void onRemoveStudent(Student student, int position);

        void onSnackBarDismissed();

        void onSnackBarActionClick();

        void on2Click();

        void addStudent();

        void saveStudentsCount();
    }

    interface StudentRemoveListener {
        void onRemoveStudent(Student student, int position);
    }

    interface Listener2 {
        void onClick(Student student);
    }
}
