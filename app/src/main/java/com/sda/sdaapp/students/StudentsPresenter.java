package com.sda.sdaapp.students;

import android.util.SparseArray;

import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.data.PrefsManager;
import com.sda.sdaapp.data.Student;
import com.sda.sdaapp.room.SdaDatabase;

import java.util.ArrayList;
import java.util.List;

public class StudentsPresenter implements StudentsContract.Presenter {

    private StudentsContract.View view;
    private PrefsManager prefsManager;
    private StudentDao studentDao;
    private List<Student> students = new ArrayList<>();
    private SparseArray<Student> removedStudent = new SparseArray<>();
    private boolean shouldUndo = false;

    public StudentsPresenter(
            StudentsContract.View view,
            PrefsManager prefsManager,
            StudentDao studentDao
    ) {
        this.view = view;
        this.prefsManager = prefsManager;
        this.studentDao = studentDao;
    }

    @Override
    public void getStudentsCountFromSharedPreferences() {
        int studentsCount = prefsManager.getStudentsCount();
        /*for (int i = 0; i < studentsCount; i ++) {
            students.add(new Student());
        }
        view.updateList(students);*/
    }

    @Override
    public void getStudentsFromRoom() {
        view.updateList(studentDao.getAll());
    }

    @Override
    public void onRemoveStudent(Student student, int position) {
        // temporarily save reference to removed student
        removedStudent.append(position, student);

        // show snack bar
        view.showStudentRemoved();
    }

    @Override
    public void onSnackBarActionClick() {
        shouldUndo = true;
    }

    @Override
    public void onSnackBarDismissed() {
        int position = removedStudent.keyAt(0);
        Student student = removedStudent.valueAt(0);

        if (shouldUndo) {
            shouldUndo = false;
            removedStudent.clear();
            view.undoRemoval(position, student);
        } else {
            // remove student from db
            studentDao.delete(student);
        }
    }

    @Override
    public void on2Click() {
        view.showToast();
    }

    @Override
    public void addStudent() {
        studentDao.insert(new Student());
        view.updateList(studentDao.getAll());
    }

    @Override
    public void saveStudentsCount() {
        prefsManager.saveStudentsCount(students.size());
    }
}
