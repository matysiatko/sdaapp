package com.sda.sdaapp.students;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sda.sdaapp.R;
import com.sda.sdaapp.data.Student;
import com.sda.sdaapp.students.edit.EditStudentActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.StudentsHolder> {

    private StudentsContract.StudentRemoveListener removeListener;
    private StudentsContract.Listener2 listener2;
    private List<Student> studentList = new ArrayList<>();
    private int selectedItemPosition = -1;

    public StudentsAdapter(StudentsContract.StudentRemoveListener removeListener,
                           StudentsContract.Listener2 listener2) {
        this.removeListener = removeListener;
        this.listener2 = listener2;
    }

    public void updateStudentsList(List<Student> students) {
        studentList.clear();
        studentList.addAll(students);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StudentsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // this method creates a view for an item (cell)
        // get layout inflater instance via static method from passing context obtained from parent
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // create (inflate) a new view
        View studentsHolderView = layoutInflater.inflate(R.layout.item_student, parent, false);
        // return StudentsHolder instance passing previously created view
        return new StudentsHolder(studentsHolderView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentsHolder holder, int position) {
        // this method is called when the view item is being recycled
        holder.setupStudent(studentList.get(position), removeListener, listener2);
    }

    @Override
    public int getItemCount() {
        // this informs adapter how big the data set is
        return studentList.size();
    }

    public void undoRemoval(int position, Student student) {
        studentList.add(position, student);
        notifyItemInserted(position);
    }

    class StudentsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_student_id)
        TextView studentName;

        @BindView(R.id.item_student_remove_icon)
        ImageView removeIcon;

        public StudentsHolder(View studentsHolderView) {
            super(studentsHolderView);
            ButterKnife.bind(this, studentsHolderView);
        }

        public void setupStudent(
                Student student,
                StudentsContract.StudentRemoveListener listener,
                StudentsContract.Listener2 listener2
        ) {
            if (selectedItemPosition == getAdapterPosition()) {
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color
                        .colorAccent));
            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color
                        .white));
            }

            String name = student.name + " " + student.surname;
            studentName.setText(name);

            removeIcon.setOnClickListener(view -> {
                if (getAdapterPosition() == selectedItemPosition) {
                    selectedItemPosition = -1;
                }
                studentList.remove(student);
                listener.onRemoveStudent(student, getAdapterPosition());
                listener2.onClick(student);
                notifyItemRemoved(getAdapterPosition());
            });

            itemView.setOnClickListener(view -> {
                // previously we just selected the student, now we open edit screen
                Intent intent = new Intent(itemView.getContext(), EditStudentActivity.class);
                intent.putExtra(EditStudentActivity.STUDENT_ID_KEY, studentList.get(getAdapterPosition()).id);
                itemView.getContext().startActivity(intent);
            });
        }

        private void handleSelectingStudent() {
            // get position of a clicked item
            int currentPosition = getAdapterPosition();

            if (currentPosition == selectedItemPosition) {
                // clicked item is the selected one, deselect it
                selectedItemPosition = -1;
                notifyItemChanged(currentPosition);
            } else {
                // deselect previous item, and select this one
                final int previouslySelectedItem = selectedItemPosition;
                selectedItemPosition = -1;
                notifyItemChanged(previouslySelectedItem);
                selectedItemPosition = currentPosition;
                notifyItemChanged(currentPosition);
            }
        }
    }
}
