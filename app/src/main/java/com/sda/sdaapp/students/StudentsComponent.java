package com.sda.sdaapp.students;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {StudentsModule.class})
public interface StudentsComponent {

    void inject(StudentsActivity studentsActivity);
}
