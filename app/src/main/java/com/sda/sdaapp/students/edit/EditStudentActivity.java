package com.sda.sdaapp.students.edit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.RadioButton;

import com.sda.sdaapp.R;
import com.sda.sdaapp.SdaApplication;
import com.sda.sdaapp.data.Student;
import com.sda.sdaapp.students.edit.di.EditStudentModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditStudentActivity extends AppCompatActivity implements EditStudentContract.View {

    public static final String STUDENT_ID_KEY = "student id";

    @BindView(R.id.edit_student_coordinator)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.edit_student_name)
    EditText name;

    @BindView(R.id.edit_student_surname)
    EditText surname;

    @BindView(R.id.edit_student_account)
    EditText accountValue;

    @BindView(R.id.edit_student_grade)
    EditText grade;

    @BindView(R.id.edit_student_city)
    EditText city;

    @BindView(R.id.edit_student_street)
    EditText street;

    @BindView(R.id.edit_student_number)
    EditText homeNumber;

    @BindView(R.id.edit_student_is_drunk)
    RadioButton isDrunk;

    @OnClick(R.id.edit_student_save)
    public void onSaveClick() {
        presenter.onSaveClick(
                name.getText().toString(),
                surname.getText().toString(),
                accountValue.getText().toString(),
                grade.getText().toString(),
                city.getText().toString(),
                street.getText().toString(),
                homeNumber.getText().toString(),
                isDrunk.isChecked()
        );
    }

    @OnClick(R.id.edit_student_is_drunk)
    public void onIsDrunkClick() {
        isDrunkSelected = !isDrunkSelected;
        isDrunk.setChecked(isDrunkSelected);
    }

    @Inject
    EditStudentContract.Presenter presenter;

    private boolean isDrunkSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);
        ButterKnife.bind(this);

        ((SdaApplication) getApplication())
                .getAppComponent()
                .plus(new EditStudentModule(this))
                .inject(this);

        presenter.handleStudent(getIntent().getIntExtra(STUDENT_ID_KEY, -1));
    }

    @Override
    public void showMissingAddress() {
        Snackbar.make(
                coordinatorLayout,
                getString(R.string.missing_address),
                Snackbar.LENGTH_SHORT
        ).show();
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void showError() {
        Snackbar.make(
                coordinatorLayout,
                getString(R.string.student_add_error),
                Snackbar.LENGTH_SHORT
        ).show();
    }

    @Override
    public void showStudentData(Student student) {
        name.setText(student.name);
        surname.setText(student.surname);
        accountValue.setText(String.valueOf(student.accountValue));
        grade.setText(String.valueOf(student.grade));
        city.setText(student.address.city);
        street.setText(student.address.street);
        homeNumber.setText(String.valueOf(student.address.number));
        isDrunk.setChecked(student.isDrunk);
    }
}
