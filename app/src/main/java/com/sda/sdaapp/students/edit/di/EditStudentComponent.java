package com.sda.sdaapp.students.edit.di;

import com.sda.sdaapp.students.edit.EditStudentActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {EditStudentModule.class})
public interface EditStudentComponent {

    void inject(EditStudentActivity editStudentActivity);
}
