package com.sda.sdaapp.students.edit;

import com.sda.sdaapp.data.Student;

public interface EditStudentContract {

    interface View {

        void showMissingAddress();

        void close();

        void showError();

        void showStudentData(Student student);
    }

    interface Presenter {

        void onSaveClick(String name, String surname, String accountValue, String grade,
                         String city, String street, String homeNumber, boolean isDrunk);

        void handleStudent(int studentId);
    }
}
