package com.sda.sdaapp.students;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.sda.sdaapp.R;
import com.sda.sdaapp.SdaApplication;
import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.data.PrefsManager;
import com.sda.sdaapp.data.Student;
import com.sda.sdaapp.students.edit.EditStudentActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentsActivity extends AppCompatActivity implements StudentsContract.View,
        StudentsContract.StudentRemoveListener {

    @BindView(R.id.students_recycler)
    RecyclerView studentsRecycler;

    @BindView(R.id.students_coordinator)
    CoordinatorLayout studentsCoordinator;

    @OnClick(R.id.students_fab)
    public void onFabClick() {
        startActivity(new Intent(this, EditStudentActivity.class));
    }

    @Inject
    StudentsContract.Presenter presenter;

    private StudentsAdapter studentsAdapter;
    private Snackbar snackbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
        ((SdaApplication) getApplication()).getAppComponent()
                .plus(new StudentsModule(this))
                .inject(this);

        ButterKnife.bind(this);

        setupRecycler();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.getStudentsFromRoom();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.saveStudentsCount();
    }

    @Override
    public void updateList(List<Student> students) {
        studentsAdapter.updateStudentsList(students);
    }

    @Override
    public void onRemoveStudent(Student student, int position) {
        presenter.onRemoveStudent(student, position);
    }

    @Override
    public void showStudentRemoved() {
        if (snackbar == null) {
            snackbar = Snackbar.make(
                    studentsCoordinator,
                    R.string.student_removed_message,
                    Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.student_undo), view -> presenter
                            .onSnackBarActionClick())
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            presenter.onSnackBarDismissed();
                        }
                    });
        }
        snackbar.show();
    }

    @Override
    public void undoRemoval(int position, Student student) {
        studentsAdapter.undoRemoval(position, student);
    }

    @Override
    public void showToast() {
        Toast.makeText(this, "Asdad", Toast.LENGTH_SHORT).show();
    }

    private void setupRecycler() {
        studentsRecycler.setLayoutManager(new LinearLayoutManager(this));

        studentsAdapter = new StudentsAdapter(this, student -> presenter.on2Click());
        studentsRecycler.setAdapter(studentsAdapter);

        RecyclerView.ItemDecoration decoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL);
        studentsRecycler.addItemDecoration(decoration);
    }
}
