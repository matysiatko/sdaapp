package com.sda.sdaapp.students.edit.di;

import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.students.edit.EditStudentContract;
import com.sda.sdaapp.students.edit.EditStudentPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EditStudentModule {

    private EditStudentContract.View view;

    public EditStudentModule(EditStudentContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    EditStudentContract.Presenter provideEditStudentPresenter(StudentDao studentDao) {
        return new EditStudentPresenter(view, studentDao);
    }
}
