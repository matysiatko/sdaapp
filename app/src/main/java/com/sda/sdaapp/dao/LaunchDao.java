package com.sda.sdaapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.sda.sdaapp.data.Launch;

import java.util.List;

@Dao
public interface LaunchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLaunches(Launch... launches);

    @Query("SELECT * FROM launches")
    List<Launch> getAllLanches();

    @Query("SELECT * FROM launches WHERE launch_year >= :year")
    List<Launch> getAllLaunchesSinceYear(int year);
}
