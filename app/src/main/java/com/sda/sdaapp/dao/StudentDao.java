package com.sda.sdaapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.sda.sdaapp.data.Student;

import java.util.List;

@Dao
public interface StudentDao {

    @Query("SELECT * FROM Student")
    List<Student> getAll();

    @Query("SELECT * FROM Student WHERE id LIKE :studentId LIMIT 1")
    Student getStudentById(int studentId);

    @Insert
    long insert(Student student);

    @Delete
    void delete(Student student);

    @Update
    int update(Student student);
}
