package com.sda.sdaapp.camera;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.sda.sdaapp.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CameraActivity extends AppCompatActivity {

    public static final int CAMERA_REQUEST = 12312;
    public static final int CAMERA_PERMISSION_REQUEST = 12322;
    public static final String PHOTO_NAME = "my_photo.jpg";
    public static final String PHOTOS_DIR = "my_photos";

    Button cameraButton;
    Button loadPhotoButton;
    ImageView cameraImage;
    Context thisActivity = this;
    private boolean hasDenied = false;
    Bitmap bitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        cameraButton = (Button) findViewById(R.id.camera_button);
        loadPhotoButton = (Button) findViewById(R.id.camera_button_load);
        cameraImage = (ImageView) findViewById(R.id.camera_image);

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission(thisActivity);
            }
        });

        loadPhotoButton.setOnClickListener(view -> {
            loadPhotoFromInternalStorage();
        });

        findViewById(R.id.camera_button_process_with_ml).setOnClickListener(view -> {
            if (bitmap != null) {
                sendBitmapToMLProcessing(bitmap);
            } else {
                Toast.makeText(this, "Provide the image!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendBitmapToMLProcessing(Bitmap bitmap) {
        Intent intent = new Intent(this, PhotoMLActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(PhotoMLActivity.IMAGE_KEY, bitmap);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void savePhotoInInternalStorage(Bitmap bitmap) {
        File photoDirectory = getDir(PHOTOS_DIR, Context.MODE_PRIVATE);
        File photo = new File(photoDirectory, PHOTO_NAME);
        try {
            FileOutputStream outputStream = new FileOutputStream(photo);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPhotoFromInternalStorage() {
        File photoDirectory = getDir(PHOTOS_DIR, Context.MODE_PRIVATE);
        File photo = new File(photoDirectory, PHOTO_NAME);
        try {
            FileInputStream inputStream = new FileInputStream(photo);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            this.bitmap = bitmap;
            cameraImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED) {
            // happy path
            openCamera();
        } else {
            // we don't have the permission
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                    .CAMERA)) {
                // already asked, used denied
                // explain to the user why you need this permission
                showAlertDialogToTheUser();
            } else {
                // used chose "don't ask again" or we are asking for the first time
                if (!hasDenied) {
                    hasDenied = true;
                    requestCameraPermission();
                } else {
                    showAlertDialogToTheUser();
                }
            }
        }
    }

    private void showAlertDialogToTheUser() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.dialog_title);
        dialogBuilder.setMessage(R.string.permission_denied_message);
        dialogBuilder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // navigate to settings screen where user can grant permissions manually
                goToSettingsScreen();
            }
        });
        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // just dismiss the dialog
            }
        });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    private void goToSettingsScreen() {
        startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts
                ("package", getPackageName(), null)));
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                CAMERA_PERMISSION_REQUEST);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, CAMERA_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                // show thumbnail in our image view
                Bundle bundle = data.getExtras();
                bitmap = (Bitmap) bundle.get("data");
                cameraImage.setImageBitmap(bitmap);
                savePhotoInInternalStorage(bitmap);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // user has granted the requested permission
                openCamera();
            } else {
                // user denied, show a message
                Toast.makeText(this, getString(R.string.permission_denied_message), Toast
                        .LENGTH_SHORT).show();
            }
        }
    }
}
