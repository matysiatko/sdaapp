package com.sda.sdaapp.camera

import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.sda.sdaapp.R
import com.sda.sdaapp.R.id.photo_ml_image
import com.sda.sdaapp.R.id.photo_ml_values
import kotlinx.android.synthetic.main.activity_photo_ml.*

class PhotoMLActivity : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_ml)
        
        analyzeImageWithMachineLearning(intent.extras.getParcelable(IMAGE_KEY))
    }
    
    private fun analyzeImageWithMachineLearning(bitmap: Bitmap) {
        val rotatedHorizontally = rotatePhoto(bitmap, 270f)
        photo_ml_image.setImageBitmap(rotatedHorizontally)
        recognizeText(rotatedHorizontally)
        recognizeFace(bitmap)
    }
    
    private fun rotatePhoto(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }
    
    private fun recognizeText(bitmap: Bitmap) {
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val detector = FirebaseVision.getInstance().visionTextDetector
        
        detector.detectInImage(image)
            .addOnSuccessListener { firebaseVisionText ->
                var result = ""
                if (firebaseVisionText.blocks.isEmpty()) {
                    result = "No text found\n"
                } else {
                    firebaseVisionText.blocks.forEach {
                        it.lines.forEach {
                            it.elements.forEach {
                                result += " ${it.text}"
                            }
                        }
                    }
                }
                
                photo_ml_values.text = result
            }
            .addOnFailureListener(this@PhotoMLActivity::onFailure)
            .addOnCanceledListener {
                onCancelled()
            }
    }
    
    private fun recognizeFace(bitmap: Bitmap) {
        val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setModeType(FirebaseVisionFaceDetectorOptions.ACCURATE_MODE)
            .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
            .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
            .setMinFaceSize(0.15f)
            .setTrackingEnabled(true)
            .build()
        
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        
        FirebaseVision.getInstance().getVisionFaceDetector(options).run {
            var result = "${photo_ml_values.text}\n\n"
            
            detectInImage(image)
                .addOnSuccessListener { faces ->
                    faces.forEach { face ->
                        face.apply {
                            result += "Probability of:\n"
                            result += "Smiling: $smilingProbability \n"
                            result += "Left eye open: $leftEyeOpenProbability \n"
                            result += "Right eye open: $rightEyeOpenProbability \n"
                        }
                    }
    
                    photo_ml_values.text = result
                }
                .addOnFailureListener(this@PhotoMLActivity::onFailure)
                .addOnCanceledListener {
                    onCancelled()
                }
        }
    }
    
    private fun onFailure(exception: Exception) {
        photo_ml_values.text = exception.message
    }
    
    private fun onCancelled() {
        photo_ml_values.text = "Cancelled"
    }
    
    companion object {
        const val IMAGE_KEY = "photo bitmap"
        private const val TAG = "VISION DETECTOR"
    }
}