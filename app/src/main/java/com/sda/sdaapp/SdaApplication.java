package com.sda.sdaapp;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.sda.sdaapp.di.AppComponent;
import com.sda.sdaapp.di.AppModule;
import com.sda.sdaapp.di.DaggerAppComponent;
import com.sda.sdaapp.di.DataModule;
import com.sda.sdaapp.room.SdaDatabase;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;


public class SdaApplication extends Application {

    private static SdaDatabase database;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return super.createStackElementTag(element) + " *** " + element.getLineNumber();
                }
            });
        }

        // Room
        database = Room.databaseBuilder(this, SdaDatabase.class, "sda-room-database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        // Realm
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        // Dagger 2
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule())
                .build();
    }

    public static SdaDatabase getRoom() {
        return database;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
