package com.sda.sdaapp.api;

import com.sda.sdaapp.data.CompanyInfo;
import com.sda.sdaapp.data.Launch;
import com.sda.sdaapp.data.Rocket;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    String BASE_URL = "https://api.spacexdata.com/v2/";

    @GET("info")
    Flowable<CompanyInfo> getCompanyInfo();

    @GET("rockets")
    Single<List<Rocket>> getRockets();

    @GET("launches")
    Single<List<Launch>> getLaunchesByYear(@Query("launch_year") int launchYear);

    @GET("launches")
    Observable<List<Launch>> getAllLaunchesForRocket(@Query("rocket_id") String rocketId);
}