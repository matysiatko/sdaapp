package com.sda.sdaapp.api;

public interface SpacexAPI {

    String BASE_URL = "https://api.spacexdata.com/v2";

    String COMPANY_INFO = BASE_URL + "/info";

    String ROCKETS = BASE_URL + "/rockets";
}
