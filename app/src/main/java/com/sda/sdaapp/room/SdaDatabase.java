package com.sda.sdaapp.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.sda.sdaapp.dao.LaunchDao;
import com.sda.sdaapp.dao.StudentDao;
import com.sda.sdaapp.data.Launch;
import com.sda.sdaapp.data.Student;

@Database(entities = {Student.class}, version = 1)
public abstract class SdaDatabase extends RoomDatabase {

    public abstract StudentDao studentDao();
}
