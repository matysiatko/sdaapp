package com.sda.sdaapp;

public interface TextCommunicator {

    void communicate(String textToCommunicate);
}
